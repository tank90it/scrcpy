package com.genymobile.scrcpy;

import android.os.Build;
import android.os.SystemClock;
import android.view.InputDevice;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MotionEvent;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class WinControl {
    private static final int DEVICE_ID_VIRTUAL = -1;

    private static final ScheduledExecutorService EXECUTOR = Executors.newSingleThreadScheduledExecutor();
    private Device mDevice;
    private boolean keepPowerModeOff;
    private final KeyCharacterMap charMap = KeyCharacterMap.load(KeyCharacterMap.VIRTUAL_KEYBOARD);
    private long lastTouchDown;
    private final PointersState pointersState = new PointersState();
    private final MotionEvent.PointerProperties[] pointerProperties = new MotionEvent.PointerProperties[PointersState.MAX_POINTERS];
    private final MotionEvent.PointerCoords[] pointerCoords = new MotionEvent.PointerCoords[PointersState.MAX_POINTERS];
    private int mScreenWidth = 1080;
    private int mScreenHeight = 1920;

    public WinControl(Options options) {
        mDevice = new Device(options);
        initPointers();
    }

    private void initPointers() {
        for (int i = 0; i < PointersState.MAX_POINTERS; ++i) {
            MotionEvent.PointerProperties props = new MotionEvent.PointerProperties();
            props.toolType = MotionEvent.TOOL_TYPE_FINGER;

            MotionEvent.PointerCoords coords = new MotionEvent.PointerCoords();
            coords.orientation = 0;
            coords.size = 1;

            pointerProperties[i] = props;
            pointerCoords[i] = coords;
        }
    }

    public void setScreenSize(int width, int height) {
        mScreenWidth = width;
        mScreenHeight = height;
    }

    private boolean injectKeycode(int action, int keycode, int repeat, int metaState) {
        if (keepPowerModeOff && action == KeyEvent.ACTION_UP && (keycode == KeyEvent.KEYCODE_POWER || keycode == KeyEvent.KEYCODE_WAKEUP)) {
            schedulePowerModeOff();
        }
        return mDevice.injectKeyEvent(action, keycode, repeat, metaState);
    }

    public void selectAll() {
        injectKeycode(0, 113, 0, 2109440);
        injectKeycode(0, 29, 0, 2109440);
        injectKeycode(1, 29, 0, 2109440);
        injectKeycode(1, 113, 0, 2097152);
        keepCtrl(29);
    }

    public void moveCursorToEnd() {
        keepCtrl(22);
    }

    public void keepCtrl(int code) {
        injectKeycode(0, 113, 0, 2109440);
        injectKeycode(0, code, 0, 2109440);
        injectKeycode(1, code, 0, 2109440);
        injectKeycode(1, 113, 0, 2097152);
    }

    public boolean injectKeycode(int keycode) {
        return mDevice.injectKeycode(keycode);
    }

    private boolean injectChar(char c) {
        String decomposed = KeyComposition.decompose(c);
        char[] chars = decomposed != null ? decomposed.toCharArray() : new char[]{c};
        KeyEvent[] events = charMap.getEvents(chars);
        if (events == null) {
            return false;
        }
        for (KeyEvent event : events) {
            if (!mDevice.injectEvent(event)) {
                return false;
            }
        }
        return true;
    }
    public static int randomNumber(int min, int max) {
        return new Random().nextInt(max - min) + min;
    }
    public int injectText(String text) {
        int successCount = 0;
        for (char c : text.toCharArray()) {
            if (!injectChar(c)) {
                Ln.w("Could not inject char u+" + String.format("%04x", (int) c));
                continue;
            }
            successCount++;
        }
        return successCount;
    }

    public static JSONObject getMapping() {
        JSONObject mapping = new JSONObject();
        try {
            mapping.put("A", 29);
            mapping.put("B", 30);
            mapping.put("C", 31);
            mapping.put("D", 32);
            mapping.put("E", 33);
            mapping.put("F", 34);
            mapping.put("G", 35);
            mapping.put("H", 36);
            mapping.put("I", 37);
            mapping.put("J", 38);
            mapping.put("K", 39);
            mapping.put("L", 40);
            mapping.put("M", 41);
            mapping.put("N", 42);
            mapping.put("O", 43);
            mapping.put("P", 44);
            mapping.put("Q", 45);
            mapping.put("R", 46);
            mapping.put("S", 47);
            mapping.put("T", 48);
            mapping.put("U", 49);
            mapping.put("V", 50);
            mapping.put("W", 51);
            mapping.put("X", 52);
            mapping.put("Y", 53);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mapping;
    }

    public static JSONObject KEYCODES = getMapping();

    public int injectText2(String text) {
        String[] characters = text.split("");
        for (String c : characters) {
            boolean isUpper = true;
            // Default upper case
            int keycode = KEYCODES.optInt(c, -1);
            if (keycode == -1) { // lower case
                isUpper = false;
                keycode = KEYCODES.optInt(c.toUpperCase(), -1);
            }
            if (keycode == -1) {
                injectText(c);
            } else {
                int metaState = isUpper ? 3145728 : 2097152;
                injectKeycode(0, keycode, 0, metaState);
                sleep(randomNumber(60, 100));
                injectKeycode(1, keycode, 0, metaState);
            }
            sleep(randomNumber(30, 60));
        }
        return 0;
    }

    public static void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void injectTap(int x, int y) {
        injectTapDown(x, y);
        injectTapUp(x, y);
    }

    public void injectLongTap(int x, int y) {
        injectTapDown(x, y);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        injectTapUp(x, y);
    }

    private float randomPressure() {
        return (float) ((double) ThreadLocalRandom.current().nextInt(1, 10)) / 10;
    }

    public boolean injectTapDown(int x, int y) {
        Position position = new Position(x, y, mScreenWidth, mScreenHeight);
        return injectTouch(0, -1, position, randomPressure(), 1);
    }

    public boolean injectTapUp(int x, int y) {
        Position position = new Position(x, y, mScreenWidth, mScreenHeight);
        return injectTouch(1, -1, position, randomPressure(), 1);
    }

    private boolean injectTouch(int action, long pointerId, Position position, float pressure, int buttons) {
        long now = SystemClock.uptimeMillis();

        Point point = mDevice.getPhysicalPoint(position);
        if (point == null) {
            Ln.w("Ignore touch event, it was generated for a different device size");
            return false;
        }

        int pointerIndex = pointersState.getPointerIndex(pointerId);
        if (pointerIndex == -1) {
            Ln.w("Too many pointers for touch event");
            return false;
        }
        Pointer pointer = pointersState.get(pointerIndex);
        pointer.setPoint(point);
        pointer.setPressure(pressure);
        pointer.setUp(action == MotionEvent.ACTION_UP);

        int pointerCount = pointersState.update(pointerProperties, pointerCoords);

        if (pointerCount == 1) {
            if (action == MotionEvent.ACTION_DOWN) {
                lastTouchDown = now;
            }
        } else {
            // secondary pointers must use ACTION_POINTER_* ORed with the pointerIndex
            if (action == MotionEvent.ACTION_UP) {
                action = MotionEvent.ACTION_POINTER_UP | (pointerIndex << MotionEvent.ACTION_POINTER_INDEX_SHIFT);
            } else if (action == MotionEvent.ACTION_DOWN) {
                action = MotionEvent.ACTION_POINTER_DOWN | (pointerIndex << MotionEvent.ACTION_POINTER_INDEX_SHIFT);
            }
        }

        MotionEvent event = MotionEvent
                .obtain(lastTouchDown, now, action, pointerCount, pointerProperties, pointerCoords, 0, buttons, 1f, 1f, DEVICE_ID_VIRTUAL, 0,
                        InputDevice.SOURCE_TOUCHSCREEN, 0);
        return mDevice.injectEvent(event);
    }

    public boolean injectScroll (int x, int y, int hScroll, int vScroll) {
        Position position = new Position(x, y, mScreenWidth, mScreenHeight);
        return injectScroll(position, hScroll, vScroll);
    }

    private boolean injectScroll(Position position, int hScroll, int vScroll) {
        long now = SystemClock.uptimeMillis();
        Point point = mDevice.getPhysicalPoint(position);
        if (point == null) {
            // ignore event
            return false;
        }

        MotionEvent.PointerProperties props = pointerProperties[0];
        props.id = 0;

        MotionEvent.PointerCoords coords = pointerCoords[0];
        coords.x = point.getX();
        coords.y = point.getY();
        coords.setAxisValue(MotionEvent.AXIS_HSCROLL, hScroll);
        coords.setAxisValue(MotionEvent.AXIS_VSCROLL, vScroll);

        MotionEvent event = MotionEvent
                .obtain(lastTouchDown, now, MotionEvent.ACTION_SCROLL, 1, pointerProperties, pointerCoords, 0, 0, 1f, 1f, DEVICE_ID_VIRTUAL, 0,
                        InputDevice.SOURCE_TOUCHSCREEN, 0);
        return mDevice.injectEvent(event);
    }

    /**
     * Schedule a call to set power mode to off after a small delay.
     */
    private static void schedulePowerModeOff() {
        EXECUTOR.schedule(new Runnable() {
            @Override
            public void run() {
                Ln.i("Forcing screen off");
                Device.setScreenPowerMode(Device.POWER_MODE_OFF);
            }
        }, 200, TimeUnit.MILLISECONDS);
    }

    private boolean pressBackOrTurnScreenOn() {
        int keycode = mDevice.isScreenOn() ? KeyEvent.KEYCODE_BACK : KeyEvent.KEYCODE_POWER;
        if (keepPowerModeOff && keycode == KeyEvent.KEYCODE_POWER) {
            schedulePowerModeOff();
        }
        return mDevice.injectKeycode(keycode);
    }

    public boolean setClipboard(String text, boolean paste) {
        boolean ok = mDevice.setClipboardText(text);
        if (ok) {
            Ln.i("Device clipboard set");
        }
        // On Android >= 7, also press the PASTE key if requested
        if (paste && Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && mDevice.supportsInputEvents()) {
            mDevice.injectKeycode(KeyEvent.KEYCODE_PASTE);
        }
        return ok;
    }
}
